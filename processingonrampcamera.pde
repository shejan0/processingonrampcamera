/*
* Processing OnRamps Project
 * By Shejan Shuza, Azariel Del Carmen, and Adrian Ahumada
 * THIS PROJECT REQUIRES CONNECTED WEBCAM OR INTERNAL CAPTURE
 * Some effects maybe affected by the 
 */

import processing.video.Capture; //allows access to Cameras, and extends PImage, which allows usage of pixel array
//import processing.serial.Serial; //to implement delay() used in sections of the program to alleviate crashing and reaching of running code before the code can complete itself
Capture cam; //create object for Camera to be used
float r, g, b; //used for filters
int cameracounter=0; //used as scroller for input devices
boolean[] effects; //accessor for all effects
String[] cameras=Capture.list(); //create a global variable of the string names of all available cameras and input devices, used in GUI
boolean setimg; //which rendering pipeline to be used
boolean showGUI; //to show or not to show GUI
void setup() {
  if (cameras.length==0) { //if there are no cameras or recognizable input devices
    println("This program is meant to be run with an applicable Webcam or Internal Capture");
    exit();
  }
  fullScreen();  
  frame.requestFocus(); //from JApplet layer, meant to ask the OS for Focus, on older OSes the focus is ripped from other programs and given to this program, newer ones ask, or highlight the task
  frame.toFront(); //from JApplet layer, forces this program to go to the front of the program stack in the browser, compacted with this program being fullScreen, you should lose all access to OS.
  for (int n=0; n<cameras.length; n++) { //print out all available camera modes or input devices 
    println(cameras[n]);
  }
  cam=new Capture(this, cameras[cameracounter]);
  cam.start();  
  frameRate(30); //frame rate cap, used to alleviate rendering problems with Set()
  effects=new boolean[7]; //grayscale| invert| add blacking noise| adding color noise| mirroring bottomhalf|mirroring tophalf|glitch
}
void draw() {
  clear();
  background(204); //same background as Processing default background
  if (cam.available()) { //wait until a new frame is available from capture device then read it, then place that data into pixel array
    cam.read();
    cam.loadPixels();
  }
  while (cam.pixels==null) { //wait for frame data, keep forcing reads and loading of pixels
    cam.read();
    cam.loadPixels();
  }
  //effects rack, this is done prior to rendering such that all effects have a chance to be calculated prior to the rendering. 
  String filters="Active Filters:";
  if (effects[0]) {
    //create grayscale frame
    for (int n=0; n<cam.pixels.length; n++) {
      color c=cam.pixels[n];
      float gray=(red(c)+green(c)+blue(c))/3.0;
      cam.pixels[n]=color(gray, gray, gray);
    }
    filters+="\nGrayscale";
    cam.updatePixels();
  }
  if (effects[1]) {
    //create inverted frame
    for (int n=0; n<cam.pixels.length; n++) {
      color c=cam.pixels[n];
      r=255-red(c);
      g=255-green(c);
      b=255-blue(c);
      cam.pixels[n]=color(r, g, b);
    }
    filters+="\nInverted";
    cam.updatePixels();
  }
  if (effects[2]) {
    //grayscale noise
    boolean ouf=(random(1)==0); //if the randomizer is 0 from a space of 0 to 1
    for (int n=0; n<cam.pixels.length; n++) {
      color c=cam.pixels[n];
      float s=random(64);
      if (ouf) {
        r=red(c)+s;
        g=green(c)+s;
        b=blue(c)+s;
      } else {
        r=red(c)-s;
        g=green(c)-s;
        b=blue(c)-s;
      }
      cam.pixels[n]=color(r, g, b);
    }
    filters+="\nGrayScale Noise";
    cam.updatePixels();
  }
  if (effects[3]) {
    //color noise
    boolean ouf=(random(1)==0); //if the randomizer is 0 from a space of 0 to 1
    for (int n=0; n<cam.pixels.length; n++) {
      color c=cam.pixels[n];
      if (ouf) {
        r=red(c)+random(128);
        g=green(c)+random(128);
        b=blue(c)+random(128);
      } else {
        r=red(c)-random(128);
        g=green(c)-random(128);
        b=blue(c)-random(128);
      }
      cam.pixels[n]=color(r, g, b);
    }
    filters+="\nRGB Noise";
    cam.updatePixels();
  }
  //glitch
  if (effects[4]) {
    for (int n=0; n<cam.pixels.length; n++) {
      cam.pixels[n]=cam.pixels[(cam.pixels.length/2)-n/2];
      cam.pixels[n/2]=cam.pixels[(cam.pixels.length/2)-n/2];
    }
    filters+="\nGlitch Effect";
    cam.updatePixels();
  }
  //corrupted
  if (effects[5]) {
    for (int n=0; n<cam.pixels.length/2; n++) {
      /*cam.pixels[n*2]=cam.pixels[(cam.pixels.length/2)+n/2];
      cam.pixels[n]=cam.pixels[(cam.pixels.length/2)+n/2];
      cam.pixels[n/2]=cam.pixels[(cam.pixels.length/2)+n/2];*/
      //effect reduced to be more understanadable, uncomment above for more strong effect
      cam.pixels[n/3]=cam.pixels[(cam.pixels.length/3)+n/3];
      cam.pixels[n/4]=cam.pixels[(cam.pixels.length/4)+n/4];
      cam.pixels[n*2]=cam.pixels[(cam.pixels.length/2)+n/2]; //setting a pixel location to another pixel location does not replace, rather adds the colors together
      cam.pixels[n]=cam.pixels[(cam.pixels.length/2)-n/2];
    }
    filters+="\nCorruption";
    cam.updatePixels();
  }
  //mirror room effect
  if (effects[6]) {
    for (int n=0; n<cam.pixels.length/2; n++) {
      cam.pixels[n*2]=cam.pixels[(cam.pixels.length/2)+n]; //setting a pixel location to another pixel location does not replace, rather adds the colors together
    }
    filters+="\nMirror Room";
    cam.updatePixels();
  }
  if (showGUI) { //boost the camera colors if the GUI is shown
    //the reason that this is BEFORE rendering camera is that the pixels are boosted prior to rendering, and the GUI is rendered after
    for (int n=0; n<cam.pixels.length; n++) {
      color c=cam.pixels[n];
      cam.pixels[n]=color(red(c)+128, green(c)+128, blue(c)+128);
    }
  }
  //resolve average grayscale color, to set text Color
  if (setimg) {
    fill(0);
  } else {
    float gray=0.0;
    for (int n=0; n<cam.pixels.length; n++) {
      color c=cam.pixels[n];
      gray+=(red(c)+green(c)+blue(c))/3.0;
    }
    if (gray/cam.pixels.length>=60)
      fill(0);
    else
      fill(255);
  }
  //render camera to screen
  if (setimg) {
    set(0, 0, cam);
    delay(100); //removed some rendering problems with Set, 
    text("You are currently using Set renderer, this renderer may have issues with certain effects, but runs faster", width/1.5, 430, width-(width/1.5), height);
  } else {
    image(cam, 0, 0, width, height);
    text("You are currently using Image renderer, this will affect performance, but has FullScreen", width/1.5, 430, width-(width/1.5), height);
  }
  textSize(16);
  text("Press G to toggle GUI", width/1.4, height-50, width-(width/1.5), height);
  //GUI Rack, render GUI elements above the camera
  if (showGUI) {
    for (int n=0; n<cam.pixels.length; n++) {
      color c=cam.pixels[n];
      cam.pixels[n]=color(red(c)+128, green(c)+128, blue(c)+128);
    }
    cam.updatePixels();
    fill(0);
    textSize(32);
    textAlign(LEFT);
    text("OnRamps Filter Project", width/1.5, 40);
    textSize(24);
    text("Developed by Shejan Shuza, Azariel Del Carmen, and Adrian Ahmuda", width/1.5, 60, width-(width/1.5), height);
    textSize(16);
    text("Number Row 1 through 7: Filters", width/1.5, 200, width-(width/1.5), height);
    text("F key: Change rendering pipeline (FullScreen Support)", width/1.5, 220, width-(width/1.5), height);
    text("S key: save current picture", width/1.5, 240, width-(width/1.5), height);
    text("J Key: Move backward in Camera List", width/1.5, 260, width-(width/1.5), height);
    text("L Key: Move forward in Camera List", width/1.5, 280, width-(width/1.5), height);
    text("Camera List is displayed in Processing Window Console", width/1.5, 300, width-(width/1.5), height);
    text("Your current selected camera is: ", width/1.5, 360, width-(width/1.5), height);
    text(cameras[cameracounter], width/1.5, 380, width-(width/1.5), height);
    text(filters, width/1.5, 520, width-(width/1.5), height);
  }
}
    void keyPressed() {
      if (key=='g') {
        showGUI=!showGUI;
      }
      if (key=='1') {
        effects[0]=!effects[0];
      }
      if (key=='2') {
        effects[1]=!effects[1];
      }
      if (key=='3') {
        effects[2]=!effects[2];
      }
      if (key=='4') {
        effects[3]=!effects[3];
      }

      if (key=='5') {
        effects[4]=!effects[4];
      }
      if (key=='6') {
        effects[5]=!effects[5];
      }
      if (key=='7'){
        effects[6]=!effects[6];
      }
      if (key=='j') {
        if (cameracounter==0) {
          cameracounter=cameras.length-1;
          effects=new boolean[7];
          cam.stop();
          cam=new Capture(this, cameras[cameracounter]);
          cam.start();
        } else {
          effects=new boolean[7];
          cam.stop();
          cameracounter--;
          cam=new Capture(this, cameras[cameracounter]);
          cam.start();
        }
      }
      if (key=='l') {
        if (cameracounter==cameras.length-1) {
          cameracounter=0;
          cam.stop();
          cam=new Capture(this, cameras[cameracounter]);
          cam.start();
        } else {
          cam.stop();
          cameracounter++;
          cam=new Capture(this, cameras[cameracounter]);
          cam.start();
        }
      }
      if (key=='s') {
        if (!cam.save("OnRampsFilter-"+month()+"_"+day()+"_"+year()+"-"+hour()+"_"+minute()+"_"+second())) {
          println("Saving mechanism returned false");
          effects[1]=!effects[1];
          delay(500);
          effects[1]=!effects[1];
          delay(500);
          effects[1]=!effects[1];
          delay(500);
          effects[1]=!effects[1];
        }
      }
      if (key=='f') {
        setimg=!setimg;
      }
    }

    /*
  if (keyPressed) {
     if (key=='1') {
     for (int n=0; n<cam.pixels.length; n++) {
     color c=cam.pixels[n];
     float gray=(red(c)+green(c)+blue(c))/3.0;
     cam.pixels[n]=color(gray,gray,gray);
     }
     }
     }
     */
