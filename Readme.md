This project was programmed in Processing using the Video Library to access the underlying pixel color array of the camera input and directly manipulate values. 
Since there is always new frames, all the algorithms have to work in real-time in comparison to the starter code, which was based on per action clicks and preloading an image into memory.

Capture devices are essentially devices that push pixels constantly when called for by software, however, due to the limitations of the hardware, and better in-code operation. 
These run at given framerates, and internally, shutter rates. 
Obviously, the CPU of the system and the Processing execution is significantly faster than a 30fps frame rate capture. 
Hence, the `if(cam.available()){}` is used to find if a new frame is available from the camera, and functions `cam.read();` and `cam.loadPixels();` are used consistently to add new pixels into the `cam.pixels[]` which is accessed to create the filter functions, which is incited by points inside of a `boolean[]` array.

**The Filters and User Functionality**

In order to see a quick reference sheet for the program, with the program focused (usually clicking on the program itself resolves focus issues), press **G** on the keyboard, this will toggle the GUI, which includes all the functionality instructions on-screen. 
Buttons **1** through **7** enable filters (in order)

1.  Grayscale
2.  Invert Colors
3.  Grayscale Noise (Grayscale Mosaic)
4.  RGB Noise (Color Mosaic)
5.  Glitch effect (replaced thin strips of frame)
6.  Corrupted effect (misplacing of pixels)
7.  Mirror Room effect (Transparent repeated layer of frame)

Like stated a `boolean[]` array titled effects essentially holds the true or false locations for all the effects, this way they can be individually referenced, without having to create and hold different boolean variables. 
Effect values are edited inside of the `keyPressed()` function which upon a key being pressed inside of the program initiates. 
Inside of `keyPressed`, the values are bounded to their corrective position inside of effects, such that the effect based on that position will be toggled.


The Grayscale effect creates a color object from the referenced pixel location, adds up the individual values from the red, green, and blue channels of that color object. 
Following, the answer is divided by 3, then the same pixel is set to same grayscale value:

The Invert Colors effect creates a color object from the referenced pixel location, subtracts 255 (max color of a channel) from the red, green, and blue channels, then sets red, green, and blue to the inverted values. 

The Grayscale Noise (Grayscale Noise) effect first creates a boolean based on if a random number between 0 to 1 (no decimals) is 0, then creates a color object from the referenced pixel location and a random number (from 0 to 64) to be used on the referenced pixel. 
If the boolean was true, then the random number is added. If false, then it would be subtracted.

The RGB Noise (Color Mosaic) effect first creates a boolean based on if a random number between 0 to 1 (no decimals) is 0, then creates a color object from the referenced pixel location, then 3 random numbers (from 0 to 255) are created. 
If the boolean was true, then the random numbers are added. If false, then they would be subtracted.
 
The Glitch Effect uses a loophole in Processing regarding the `cam.pixels[]` operations for `get{}` and `set{}` which upon setting a color object to a location inside of `cam.pixels[]`, the existing data is erased, and replaced with the new values that are being set. 
However using another location from the `cam.pixels[]` array (which is a get function) and setting it to another location, does not seem to remove the previous data from the pixel, rather adding or mixing the pixels together. 
This most likely means either that the get{} function handles a get from the same location, or that pixels itself are a different object type entirely, which can be incited by color to replace its values. 
Nonetheless, the effect pulls from the second half of the picture and sets it to a pixel that was from the second half. 
As well, as a pixel from the first half to something also in the first half. 
Which means as the for loop scrolls across lines, the two location switchings mess with each other, eventually leading to a glitchy effect. 

The corrupted effect builds upon the same principle of the glitch effect, however with more bounds with a third of the frame, fourth of the frame, and again halves of the frames being affected.

The mirror room effect uses only the first part of the glitch effect, which only redraws bottom half of the frame and spreads it to more of the bigger frame.

**Core Functionality**

Like stated, the CPU of the system and the Processing execution is significantly faster than a 30fps frame rate capture (for example). 
Hence, the `if(cam.available()){}` is used to find if a new frame is available from the camera, then functions `cam.read();` and `cam.loadPixels();` are used to add new frame pixels into the `cam.pixels[]` which is accessed to create the filter functions.

Then, using a `boolean[]` the filters are bounded to locations inside of the array, which when enabled will insight the various sections of filters. 
As every filter does `cam.updatePixels();` immediately after they complete, any filters after them will have the effects of prior filters still inside of the `cam.pixels[]`.  
`cam.pixels[]` itself is really an array of color objects that are accessible from reference, or using functions inside of color such as `red()`, `green()`, and `blue()`. 
Interesting, during development, it was noted that using some formation of `cam.pixels[]=cam.pixels[];` doesn’t entirely replace the color of the referenced pixel, but rather adds colors of one reference to another, which lead to new effects to be available. 
But, `cam.pixels[]` is a single dimension array (as indicated by only one set of brackets), another big limitation as there was no longer a defined “half-way” point vertically. 
Most other languages and rendering pipelines use a 2 dimension rendering pipeline. Which could be scrolled through a nested for loop such as:

`for(int n=0;n<pixelarray.length;n++){`

`for(int s=0;s<pixelarray[n].length;s++){}`

`pixelarray[n][s]=color(100,100,100);`

`//finding the middle of the frame horizontally is (pixelarray.length/2) and finding the middle of frame vertically is (pixelarray[s].length/2)`

`}`

However, there are limitations with using rasterized images in Processing other than `cam.pixels[]` being one dimension, primarily the rendering pipeline. 
Processing has multiple ways of rendering objects to the display, both in Vectorized and Rasterized forms. 
For pushing the PImage object to the display (cam being a Capture object which inherits PImage), Processing has 2 main ways of achieving this, which is `image(cam, 0, 0, width, height);` (width and height being the bounds to fill which creates full screen), and `set(0, 0, cam);` which is faster, but loses a lot of the functionality of image. 
Since all of the filters do take time to complete, sometimes the set function is actually faster than the filter system, which can cause very rapid flickering of filters not completing before set attempts rendering. 
